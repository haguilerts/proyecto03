
package wed;

import com.google.gson.Gson;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import persistencia.DAO;


@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
   
  Gson convertirJson = new Gson();
  ArrayList<Usuario> miListado= new ArrayList();  

   
    
 /////////////////////////////////////////////////////////////   
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
       System.out.println("Estas en el POST del server porque aqui estamos en JAVA"); 
        String pedido=req.getReader().readLine();// lo tomo los Json y gurdo en pedido
      
        // destrasformo el pedido(Json) a lenguaje java y guardo en una varible del tipo Usuario class
      Usuario guardado= convertirJson.fromJson(pedido,Usuario.class);
    
        System.out.println("nombre: "+guardado.getNombre());// imprimo esa variable nombre
        
        System.out.println(pedido);// imprimo el formato json
        miListado.add(guardado);
        resp.getWriter().println("Tu usuario se guardo en nuestra doPost");
        
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////
   
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     resp.getWriter().println("Estas consultando el metod doGet ");
      
     resp.getWriter().println(miListado);
     resp.getWriter().println(convertirJson.toJson(miListado));
     
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
}
